﻿using ContactInfo.Models;
using System;
using System.Configuration;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContactInfo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }




        public ActionResult Contact()
        {
            IList<Contacts> contact = new List<Contacts>();
            DataClasses1DataContext db2 = new DataClasses1DataContext();
            var query = from eachContact in db2.Contacts select eachContact;
            var contacts = query.ToList();
            foreach (var c in contacts)
            {
                contact.Add(new Contacts() { id = c.Id, name = c.name, phoneno = c.phoneno, email = c.email, houseno =c.houseno});
            }

            ViewBag.Message = "Your contact page.";


            return View(contact);
        }

        [HttpPost]
        public ActionResult Add(Contacts contact)
        {
            //string connString = ConfigurationManager.ConnectionStrings["contacts"].ToString();

            //DataContext db = new DataContext(connString);
            using (DataClasses1DataContext db2 = new DataClasses1DataContext())
            {
                Contact c = new Contact();
                c.name = contact.name;
                c.phoneno = contact.phoneno;
                c.email = contact.email;
                c.houseno = contact.houseno;



                db2.Contacts.InsertOnSubmit(c);
                db2.SubmitChanges();


            }
            return RedirectToAction("Index");


        }
        public ActionResult Details(int id)
        {
            DataClasses1DataContext db2 = new DataClasses1DataContext();
            Contacts model = db2.Contacts.Where(x => x.Id == id).Select(x =>
                                                new Contacts()
                                                {
                                                    id = x.Id,
                                                    name = x.name,
                                                    phoneno = x.phoneno,
                                                    email = x.email,
                                                    houseno = x.houseno
                                                }).SingleOrDefault();

            return View(model);
        }
        public ActionResult Edit(int id)
        {
            DataClasses1DataContext db2 = new DataClasses1DataContext();
            Contacts model = db2.Contacts.Where(x => x.Id == id).Select(x =>
                                                new Contacts()
                                                {
                                                    id = x.Id,
                                                    name = x.name,
                                                    phoneno = x.phoneno,
                                                    email = x.email,
                                                    houseno = x.houseno
                                                }).SingleOrDefault();

            return View(model);
        }
        public ActionResult Delete(int id)
        {
            DataClasses1DataContext db2 = new DataClasses1DataContext();
            Contacts model = db2.Contacts.Where(x => x.Id == id).Select(x =>
                                                new Contacts()
                                                {
                                                    id = x.Id,
                                                    name = x.name,
                                                    phoneno = x.phoneno,
                                                    email = x.email,
                                                    houseno = x.houseno
                                                }).SingleOrDefault();

            return View(model);
        }
        [HttpPost]
        [ActionName("Edit")]
        public ActionResult EditP(Contacts model)
        {
            try
            {
                DataClasses1DataContext db2 = new DataClasses1DataContext();
                Contact contact = db2.Contacts.Where(x => x.Id == model.id).Single<Contact>();
                contact.name = model.name;
                contact.phoneno = model.phoneno;
                contact.email = model.email;
                contact.houseno = model.houseno;

                db2.SubmitChanges();
                return RedirectToAction("Contact");
            }
            catch
            {
                return View(model);
            }
        }
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteP(Contacts model)
        {
            try
            {
                DataClasses1DataContext db2 = new DataClasses1DataContext();
                Contact contact = db2.Contacts.Where(x => x.Id == model.id).Single<Contact>();
                db2.Contacts.DeleteOnSubmit(contact);
                db2.SubmitChanges();
                return RedirectToAction("Contact");
            }
            catch
            {
                return View(model);
            }

        }

    }
}